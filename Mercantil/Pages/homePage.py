class Homepage:

    def __init__(self, driver):
        self.driver = driver

        self.drop_down_seguro = '/html/body/div[1]/header/div/nav/div[3]/ul/li[2]/ul/li[1]'
        self.link_hogar = '/html/body/div[1]/header/div/nav/div[3]/ul/li[2]/ul/li[1]/ul/li[1]/a/img'
        self.url_hogar = 'https://www.mercantilandina.com.ar/seguros-personales/hogar/'

    def seleccionar_seguro(self):
        self.driver.find_element_by_xpath(self.drop_down_seguro).click()

    def click_cotiza_page(self):
        self.driver.find_element_by_xpath(self.link_hogar).click()
