class SeguroHogar():

    def __init__(self, driver):
        self.driver = driver

        self.nombre_apellido = 'inputName'
        self.telefono = 'inputTel'
        self.email = 'inputemail'
        self.dropdown_vivienda = '/html/body/div[1]/div[1]/div/div/section/main/div[2]/div[9]/div[2]/form/div/div/div[2]/div[1]/select'
        self.click_vivienda = '/html/body/div[1]/div[1]/div/div/section/main/div[2]/div[9]/div[2]/form/div/div/div[2]/div[1]/select/option[3]'
        self.dropdown_superficie = '/html/body/div[1]/div[1]/div/div/section/main/div[2]/div[9]/div[2]/form/div/div/div[2]/div[2]/select'
        self.click_superficie = '/html/body/div[1]/div[1]/div/div/section/main/div[2]/div[9]/div[2]/form/div/div/div[2]/div[2]/select/option[5]'
        self.dropdown_ubicacion = '/html/body/div[1]/div[1]/div/div/section/main/div[2]/div[9]/div[2]/form/div/div/div[2]/div[3]/select'
        self.click_ubicacion = '/html/body/div[1]/div[1]/div/div/section/main/div[2]/div[9]/div[2]/form/div/div/div[2]/div[3]/select/option[2]'
        self.cotizar = 'cotizador-submit'
        self.cost = 'costo'
        self.chat_online = '/html/body/div[5]/div[1]/span/i'

    def enter_nombre_apellido(self, username):
        self.driver.find_element_by_id(self.nombre_apellido).send_keys(username)

    def enter_telefono(self, telefono):
        self.driver.find_element_by_id(self.telefono).send_keys(telefono)

    def enter_email(self, email):
        self.driver.find_element_by_id(self.email).send_keys(email)

    def select_vivienda(self):
        self.driver.find_element_by_xpath(self.dropdown_vivienda).click()
        self.driver.find_element_by_xpath(self.click_vivienda).click()

    def select_superficie(self):
        self.driver.find_element_by_xpath(self.dropdown_superficie).click()
        self.driver.find_element_by_xpath(self.click_superficie).click()

    def select_ubicacion(self):
        self.driver.find_element_by_xpath(self.dropdown_ubicacion).click()
        self.driver.find_element_by_xpath(self.click_ubicacion).click()

    def click_cotizar(self):
        self.driver.find_element_by_id(self.cotizar).click()