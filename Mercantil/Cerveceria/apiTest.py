import unittest
import requests
import json

class TestApi(unittest.TestCase):

    def test_brewerie_works(self):
      response = requests.get("https://api.openbrewerydb.org/breweries")
      assert response.status_code == 200

    def test_same_state(self):
        #Busco listar todas las cervecerias con el mismo estado = 'Alabama'
        response = requests.get('https://api.openbrewerydb.org/breweries')
        #print(response.status_code)
        #print(response.json())
        print('Lista de cervecerias que cumplen con la condicion:\n')
        for i in response.json():
            state = i['state']
            if state == 'Alabama':
                print(i)

    def test_same_state_city(self):
        response = requests.get('https://api.openbrewerydb.org/breweries')
        #Busco las cervecerias con estado = 'Alabama' y ciudad = 'Hunskville'
        print('La cerveceria que cumple con las condiciones es:\n')
        for i in response.json():
            state = i['state']
            city = i['city']
            if state == 'Alabama' and city == 'Huntsville':
                estado = i['state']
                expected = 'Alabama'
                #Valido que los valores sean correctos
                assert estado == expected
                assert i['id'] == 46
                assert i['street'] == '2600 Clinton Ave W'
                assert i['name'] != 'Trim Tab Brewing'
                assert i['name'] == 'Yellowhammer Brewery'
                assert i['phone'] == '2569755950'
                print(i)


if __name__ == '__main__':
    unittest.main()