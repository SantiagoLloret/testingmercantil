from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import unittest
import requests
from Mercantil.Pages.homePage import *
from Mercantil.Pages.seguroHogar import *

class Navegationtest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(
            "C:\\Users\\Molsvick\\PycharmProjects\\untitled1\\venv\\Drivers\\chromedriver.exe")
        cls.driver.maximize_window()
        driver = cls.driver
        driver.get('https://www.mercantilandina.com.ar/')
        ### Pantalla Principal ###
        homepage = Homepage(driver)
        # Selecciono Seguros
        homepage.seleccionar_seguro()
        homepage.click_cotiza_page()
        ### Pantalla cotizacion ###
        cotizapage = SeguroHogar(driver)
        #Espera a pagina
        try:
            element_present = EC.presence_of_element_located((By.ID, 'main'))
            WebDriverWait(driver, 5).until(element_present)
        except TimeoutException:
             print('Tiempo excedido')
        #Ingreso Nombre y apellido
        cotizapage.enter_nombre_apellido('Santiago Lloret')
        # Ingreso Telefono
        cotizapage.enter_telefono('2615399900')
        # Ingreso email
        cotizapage.enter_email('asdasdasd@hotmail.com')
        # Selecciono tipo de vivienda
        cotizapage.select_vivienda()
        # Selecciono superficie
        cotizapage.select_superficie()
        # Selecciono ubicacion
        cotizapage.select_ubicacion()
        # Clickear Cotizar
        cotizapage.click_cotizar()
        #Espera a elemento
        try:
            espera = WebDriverWait(driver, 5).until(
                EC.visibility_of_element_located((By.ID, cotizapage.cost)))
        except TimeoutException:
              print('Tiempo excedido')
        # Se guarda el html conteniendo a la tabla (Me salto mucho error de certificado y no puedo parsear la tabla)
        # BeautifulSoup es lo que utilizaria para scrappear la pagina
        #request = requests.get(homepage.url_hogar, verify=False)
        #soup = BeautifulSoup(request.text, 'html.parser')

    # Valido que el titulo sea 'Seguro de hogar - Mercantil andina'
    def test_valido_titulo(self):
        driver = self.driver
        assert  'Seguro de hogar - Mercantil andina' in self.driver.title

    # Checkeo si el numero es mayor a cero
    def test_valido_numero(self):
        driver = self.driver
        cotizapage = SeguroHogar(driver)
        string = driver.find_element_by_id("costo").text
        string = string.replace("$ ", "")
        stringToNumber = int(string)
        self.assertTrue(stringToNumber >= 0)

    # Valido existencia de Chat Online
    def test_validate_chat(self):
        driver = self.driver
        view = SeguroHogar(driver)
        self.assertTrue(view.chat_online)

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.driver.quit()
        print('Test Completed')
